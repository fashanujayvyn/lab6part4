/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Institute;

/**
 *
 * @author jayvy
 */
public class Book {
   
    public String title;
    public String author;
    
    Book(String title, String author)
    {
        this.title=title;
        this.author=author;
    }
    
}
